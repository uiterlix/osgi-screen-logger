
# OSGi Screen Logger

*an OSGi LogService implementation that logs to screen*

## Overview

The ScreenLogger is a simple OSGi LogService implementation that logs to screen, i.e. displays all log entries
in a window that appears on the local screen (local as in: where the OSGi framework runs). It's meant
specifically for development and will be of limited use in other scenario's.

Using the ScreenLogger can speed up your development, as you don't have look for log files, open or "tail"
them; all the information you need is just appearing right in front of you on the screen in real time.

Please note that log entries are *not* persisted; they're just displayed on the screen, that's all.

## Usage

Just download or build the bundle and deploy it into your OSGi framework. There are no dependencies whatsoever
(apart from the LogService interface itself of course).

The log window will appear when the bundle is started and disappear when the bundle is stopped. Log entries
will show up as soon as they are logged. Currently there is no limit to the number of entries displayed; this
might cause resource problems in the long run, but on default hardware with default JVM settings, hunderds of 
thousands of entries can be displayed without problems. 

When a new log entry is displayed, the window automatically scrolls to the bottom. Use the "lock" button to disable auto scrolling. When auto scrolling is disabled, a message will show the number of new log entries at the moment they arrive.

The 'service' column displays the service for which the entry was logged (if any). The service is represented 
by service interface(s) with which it was registered (and the bundle that registered it). The 'bundle' column
displays the bundle that logged the entry.

The information in all other columns will speak for itself. To see the stack trace associated with an exception,
double click on the exception.

## Building

A gradle build file is provided. Assuming you've gradle installed, the command "gradle assemble" will do.

## Limitations

When used with a headless (running) JVM, the bundle will not start. This is intentional, as it the sole
purpose of this bundle is to log to screen.

## Download

* Download release binaries directly from <https://bitbucket.org/pjtr/osgi-screen-logger/downloads>.
* Source is available on Bitbucket: <https://bitbucket.org/pjtr/osgi-screen-logger/src>.

##Feedback

Questions, problems, or other feedback? Please mail the author (peter dot doornbosch) at luminis dot eu, or create 
an issue at <https://bitbucket.org/pjtr/osgi-screen-logger/issues>.