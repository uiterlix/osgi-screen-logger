package net.luminis.screenlogger;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;

public class Activator implements BundleActivator {

    private ScreenLogger screenLogger;

    @Override
    public void start(BundleContext context) throws Exception {
        screenLogger = new ScreenLogger();
        context.registerService(LogService.class.getName(), new LogServiceFactory(), null);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        screenLogger.dispose();
    }

    public class LogServiceFactory implements ServiceFactory {
        @Override
        public Object getService(Bundle bundle, ServiceRegistration serviceRegistration) {
            return screenLogger.createLogService(bundle);
        }

        @Override
        public void ungetService(Bundle bundle, ServiceRegistration serviceRegistration, Object o) {
        }
    }
}
